/*! @source https://github.com/ChenWenBrian/FileSaver.js/blob/master/FileSaver.js */
var saveAs=saveAs||"undefined"!=typeof navigator&&navigator.msSaveOrOpenBlob&&navigator.msSaveOrOpenBlob.bind(navigator)||function(e){"use strict";if("undefined"==typeof navigator||!/MSIE [1-9]\./.test(navigator.userAgent)){var t=e.document,n=function(){return e.URL||e.webkitURL||e},o=t.createElementNS("http://www.w3.org/1999/xhtml","a"),r=!e.externalHost&&"download"in o,i=function(n){var o=t.createEvent("MouseEvents");o.initMouseEvent("click",!0,!1,e,0,0,0,0,0,!1,!1,!1,!1,0,null),n.dispatchEvent(o)},a=e.webkitRequestFileSystem,d=e.requestFileSystem||a||e.mozRequestFileSystem,s=function(t){(e.setImmediate||e.setTimeout)(function(){throw t},0)},u="application/octet-stream",c=0,l=[],f=function(){for(var e=l.length;e--;){var t=l[e];"string"==typeof t?n().revokeObjectURL(t):t.remove()}l.length=0},v=function(e,t,n){t=[].concat(t);for(var o=t.length;o--;){var r=e["on"+t[o]];if("function"==typeof r)try{r.call(e,n||e)}catch(i){s(i)}}},w=function(t,s){var f,w,p,y=this,m=t.type,h=!1,g=function(){var e=n().createObjectURL(t);return l.push(e),e},S=function(){v(y,"writestart progress write writeend".split(" "))},b=function(){(h||!f)&&(f=g(t)),w?w.location.href=f:window.open(f,"_blank"),y.readyState=y.DONE,S()},E=function(e){return function(){return y.readyState!==y.DONE?e.apply(this,arguments):void 0}},O={create:!0,exclusive:!1};return y.readyState=y.INIT,s||(s="download"),r?(f=g(t),o.href=f,o.download=s,i(o),y.readyState=y.DONE,void S()):(e.chrome&&m&&m!==u&&(p=t.slice||t.webkitSlice,t=p.call(t,0,t.size,u),h=!0),a&&"download"!==s&&(s+=".download"),(m===u||a)&&(w=e),d?(c+=t.size,void d(e.TEMPORARY,c,E(function(e){e.root.getDirectory("saved",O,E(function(e){var n=function(){e.getFile(s,O,E(function(e){e.createWriter(E(function(n){n.onwriteend=function(t){w.location.href=e.toURL(),l.push(e),y.readyState=y.DONE,v(y,"writeend",t)},n.onerror=function(){var e=n.error;e.code!==e.ABORT_ERR&&b()},"writestart progress write abort".split(" ").forEach(function(e){n["on"+e]=y["on"+e]}),n.write(t),y.abort=function(){n.abort(),y.readyState=y.DONE},y.readyState=y.WRITING}),b)}),b)};e.getFile(s,{create:!1},E(function(e){e.remove(),n()}),E(function(e){e.code===e.NOT_FOUND_ERR?n():b()}))}),b)}),b)):void b())},p=w.prototype,y=function(e,t){return new w(e,t)};return p.abort=function(){var e=this;e.readyState=e.DONE,v(e,"abort")},p.readyState=p.INIT=0,p.WRITING=1,p.DONE=2,p.error=p.onwritestart=p.onprogress=p.onwrite=p.onabort=p.onerror=p.onwriteend=null,e.addEventListener("unload",f,!1),y.unload=function(){f(),e.removeEventListener("unload",f,!1)},y}}("undefined"!=typeof self&&self||"undefined"!=typeof window&&window||this.content);"undefined"!=typeof module&&null!==module?module.exports=saveAs:"undefined"!=typeof define&&null!==define&&null!=define.amd&&define([],function(){return saveAs}),String.prototype.endsWithAny=function(){for(var e=Array.prototype.slice.call(arguments),t=this.toLowerCase().toString(),n=0;n<e.length;n++)if(-1!==t.indexOf(e[n],t.length-e[n].length))return!0;return!1};var saveTextAs=saveTextAs||function(e,t,n){if(t=t||"download.txt",n=n||"utf-8",e=(e||"").replace(/\r?\n/g,"\r\n"),saveAs&&Blob){var o=new Blob([e],{type:"text/plain;charset="+n});return saveAs(o,t),!0}var r=window.frames.saveTxtWindow;if(!r&&(r=document.createElement("iframe"),r.id="saveTxtWindow",r.style.display="none",document.body.insertBefore(r,null),r=window.frames.saveTxtWindow,!r&&(r=window.open("","_temp","width=100,height=100"),!r)))return window.alert("Sorry, download file could not be created."),!1;var i=r.document;i.open("text/html","replace"),i.charset=n,t.endsWithAny(".htm",".html")?(i.close(),i.body.innerHTML="\r\n"+e+"\r\n"):(t.endsWithAny(".txt")||(t+=".txt"),i.write(e),i.close());var a=i.execCommand("SaveAs",null,t);return r.close(),a};

// 主程序
$('.js-location').text('http://' + location.host + '/');

function getClipboard() {
    var pasteTarget = document.createElement("div");
    pasteTarget.contentEditable = true;
    document.body.appendChild(pasteTarget);
    pasteTarget.focus();
    document.execCommand("paste", null, null);
    var paste = pasteTarget.innerText;
    document.body.removeChild(pasteTarget);
    return paste;
}

function showCompressRate(len1, len2) {
    $('.rate').text('压缩前大小：' + len1 + '，压缩后大小：' + len2 + '。压缩率：' + (len2 * 100 / len1).toFixed(2) + '%');
}
// 打开本地文件
$('.upload').change(function() {
    if (!this.files || this.files.length === 0) return;
    var file = this.files[0];
    if (!/^js|css|html|htm$/i.test(file.name.split('.').slice(-1)[0])) {
        alert('文件格式不合要求');
        this.value = '';
        return;
    }
    var tid = $(this).parent().attr('data-target');
    var reader = new FileReader();
    reader.onload = function(e) {
        $('#' + tid).val(e.target.result);
    };
    reader.readAsText(file);
});
// 保存到本地文件
$('.save').click(function() {
    var tid = $(this).parent().attr('data-target');
    saveTextAs($('#' + tid).val(), (new Date()).toLocaleString() + '.js');
});
// uglify压缩
$('#uglify').click(function() {
    $.post('/uglify', $('#input').val(), function(o) {
        $('#output').val(o);
        showCompressRate($('#input').val().length, o.length);
    });
});
// jsPacker加密
$('#jspacker').click(function() {
    $.post('/jspacker', $('#input').val(), function(o) {
        $('#output').val(o);
        showCompressRate($('#input').val().length, o.length);
    });
});
// 美化
$('.beautify').click(function() {
    var type = $(this).attr('data-type');
    if (!type) return;
    $.post('/beautify?type=' + type, $('#output').val(), function(o) {
        $('#input').val(o);
    });
});
// 清空编辑区
$('.emptyCode').click(function() {
    $(this).siblings('.upload').val('');
    var tid = $(this).parent().attr('data-target');
    $('#' + tid).val('').focus();
});
// 粘贴到编辑区
$('.pasteCode').click(function() {
    var tid = $(this).parent().attr('data-target');
    $('#' + tid).val(getClipboard());
    return;
    $('#' + tid).focus().select();
    try { // 此API可能不好使
        if (document.execCommand('paste')) {
            alert('粘贴成功！');
        } else {
            alert('粘贴失败，请按Ctrl+V');
        }
    } catch (err) {
        console.log(err);
        alert('粘贴失败，请按Ctrl+V');
    }
});
// 复制编辑区内容
$('.copyCode').click(function() {
    var tid = $(this).parent().attr('data-target');
    $('#clipboard').val($('#' + tid).val()).focus().select();
    try { // 此API可能不好使
        if (document.execCommand('copy')) {
            alert('复制成功！');
        } else {
            $('#' + tid).focus().select();
            alert('复制失败，请按Ctrl+C');
        }
    } catch (err) {
        console.log(err);
        $('#' + tid).focus().select();
        alert('复制失败，请按Ctrl+C');
    }
    $('#clipboard').val('');
});
