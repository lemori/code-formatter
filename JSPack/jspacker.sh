#!/bin/bash
JSPACKER_PATH=$(dirname "$0")
WORK_PATH=$(dirname "$1")
filename=$(basename "$1")
extension="${filename##*.}"
filename="${filename%.*}"
cd "$WORK_PATH"
java -jar "$JSPACKER_PATH/yuicompressor-2.4.7.jar" $1 -o "$filename.yui.$extension"
perl "$JSPACKER_PATH/jsPacker.pl" -i "$filename.yui.$extension" -o "$filename.min.$extension" -e62 -f -q
rm -rf "$filename.yui.$extension"
